from django.shortcuts import render
from rdflib import Graph, Literal, RDF, URIRef
# rdflib knows about some namespaces, like FOAF
from rdflib.namespace import FOAF , XSD, Namespace

g = Graph()
g.parse("fifa19.ttl", format="ttl")

# Create your views here.
def index(request):
    print(update_photo_link(123456))
    return None

def update_photo_link(id):
  pattern = "https://cdn.sofifa.com/players/{id_1}/{id_2}/19_120.png"
  id = "0"+id if (len(id)==5) else id
  final = pattern.format(id_1=id[0:3],id_2=id[3:])
  return final

def update_club_link(url):
  id = url.split("/")[-1][:-4]
  pattern = "https://cdn.sofifa.com/teams/{id}/60.png"
  final = pattern.format(id=id)
  return final

def convert_name_to_url(name):
    name = name.replace(" ","_").replace(".","")
    return name

